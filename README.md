# cheatsheets

an evergrowing collection of cheatsheets, inspired by - [Cheat Sheets for AI, Neural Networks, Machine Learning, Deep Learning & Big Data](https://medium.com/p/cheat-sheets-for-ai-neural-networks-machine-learning-deep-learning-big-data-678c51b4b463)


[TOC]

## neural networks 101
###  basic types & graphs
[![00-nn-00](./_png/_thumbs/00-nn-00.png)](./_png/00-nn-00.png) [![00-nn-01](./_png/_thumbs/00-nn-01.png)](./_png/00-nn-01.png)

source: [the asimov institute](http://www.asimovinstitute.org/neural-network-zoo/)

###  how to choose? 
#### (according to [scikit-learn](www.scikitlearn.com))
[![01-nn-01](./_png/_thumbs/01-nn-01.png)](./_png/01-nn-01.png)

source: [peekaboo](http://peekaboo-vision.blogspot.com/2013/01/machine-learning-cheat-sheet-for-scikit.html)

####  how to choose? (according to [microsoft](www.microsoft.com))
[![01-nn-02](./_png/_thumbs/01-nn-02.png)](./_png/01-nn-02.png)

source: [microsoft azure machine learning studio](https://docs.microsoft.com/en-gb/azure/machine-learning/studio/algorithm-cheat-sheet) | 

### basic math
[![03-nn-01](./_png/_thumbs/03-nn-01.png)](./_png/03-nn-01.png)[![03-nn-02](./_png/_thumbs/03-nn-02.png)](./_png/03-nn-02.png)

## python 101
###  python basics
[![04-python-basics](./_png/_thumbs/04-python-basics.png)](./_png/04-python-basics.png)

source: [datacamp](https://www.datacamp.com/community/tutorials/python-data-science-cheat-sheet-basics) | [[high-res pdf]](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/PythonForDataScience.pdf) | [[png]](./_png/04-python-basics.png)

###  python numpy
[![06-python-numpy](./_png/_thumbs/06-python-numpy.png)](./_png/06-python-numpy.png)

source: [datacamp](https://www.datacamp.com/community/blog/python-numpy-cheat-sheet#gs.AK5ZBgE) | [[high-res pdf]](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Numpy_Python_Cheat_Sheet.pdf) | [[png]](./_png/06-python-numpy.png)

###  python scipy
[![07-python-scipy](./_png/_thumbs/07-python-scipy.png)](./_png/07-python-scipy.png)

source: [datacamp](https://www.datacamp.com/community/blog/python-scipy-cheat-sheet#gs.JDSg3OI) | [[high-res pdf]](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Python_SciPy_Cheat_Sheet_Linear_Algebra.pdf) | [[png]](./_png/07-python-scipy.png)

## data wrangling with python & r

**data wrangling:** processing data from raw to another format useful for analytics [[wiki]](https://en.wikipedia.org/wiki/Data_wrangling)

###  python pandas
[![08-python-pandas](./_png/_thumbs/08-python-pandas.png)](./_png/08-python-pandas.png)

source: [datacamp](https://www.datacamp.com/community/blog/python-pandas-cheat-sheet#gs.oundfxM) | [[high-res pdf]](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/PandasPythonForDataScience.pdf) | [[png]](./_png/08-python-pandas.png)

###  python pandas wrangling
[![09-python-pandas-wrangling01](./_png/_thumbs/09-python-pandas-wrangling01.png)](./_png/09-python-pandas-wrangling01.png)
[![09-python-pandas-wrangling02](./_png/_thumbs/09-python-pandas-wrangling02.png)](./_png/09-python-pandas-wrangling02.png)

source: [datacamp](https://www.datacamp.com/community/blog/pandas-cheat-sheet-python#gs.HPFoRIc) | [[high-res pdf]](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Python_Pandas_Cheat_Sheet_2.pdf) | [[png]](./_png/08-python-pandas.png)

###  r wrangling
[![10-r-wrangling01](./_png/_thumbs/10-r-wrangling01.png)](./_png/10-r-wrangling01.png)
[![10-r-wrangling02](./_png/_thumbs/10-r-wrangling02.png)](./_png/10-r-wrangling02.png)

source: [r-studio](www.rstudio.com) | [[high-res pdf]](https://www.rstudio.com/wp-content/uploads/2015/02/data-wrangling-cheatsheet.pdf)

## data visualization with python & r
###  python matplotlib
[![11-python-matplotlib](./_png/_thumbs/11-python-matplotlib.png)](./_png/11-python-matplotlib.png)

source: [datacamp](https://www.datacamp.com/community/blog/python-matplotlib-cheat-sheet#gs.uEKySpY) | [[high-res pdf]](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Python_Matplotlib_Cheat_Sheet.pdf) | [[png]](./_png/11-python-matplotlib.png)

###  python bokeh
[![12-python-bokeh](./_png/_thumbs/12-python-bokeh.png)](./_png/12-python-bokeh.png)

source: [datacamp](https://www.datacamp.com) | [[high-res pdf]](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Python_Bokeh_Cheat_Sheet.pdf)

###  r ggplot
[![13-r-ggplot01](./_png/_thumbs/13-r-ggplot01.png)](./_png/13-r-ggplot01.png)
[![13-r-ggplot02](./_png/_thumbs/13-r-ggplot02.png)](./_png/13-r-ggplot02.png)

source: [r-studio](www.rstudio.com) | [[high-res pdf]](https://www.rstudio.com/wp-content/uploads/2015/03/ggplot2-cheatsheet.pdf)

## python & machine learning
###  python scikit-learn
[![14-python-scikitlearn](./_png/_thumbs/14-python-scikitlearn.png)](./_png/14-python-scikitlearn.png)

source: [datacamp](https://www.datacamp.com/community/blog/scikit-learn-cheat-sheet) | [[high-res pdf]](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Scikit_Learn_Cheat_Sheet_Python.pdf) | [[png]](./_png/14-python-scikitlearn.png)

###  python keras
[![15-python-keras](./_png/_thumbs/15-python-keras.png)](./_png/15-python-keras.png)

source: [datacamp](https://www.datacamp.com/community/blog/keras-cheat-sheet#gs.DRKeNMs) | [[high-res pdf]](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Keras_Cheat_Sheet_Python.pdf) | [[png]](./_png/15-python-keras.png)

###  python pysparks
[![16-python-pysparks](./_png/_thumbs/16-python-pysparks.png)](./_png/16-python-pysparks.png)

###  tensorflow
[![17-tensorflow](./_png/_thumbs/17-tensorflow.png)](./_png/17-tensorflow.png)

source: [altoros](https://www.altoros.com/tensorflow-cheat-sheet.html)

## git
[github cheatsheet](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf) | [bitbucket cheatsheet](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet)

[![18-git-00](./_png/_thumbs/18-git-00.png)](./_png/18-git-00.png)
source & pdf: [cheat-sheets.org](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)